-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2024 at 03:10 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_course`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_about_us`
--

CREATE TABLE `table_about_us` (
  `id` int(11) NOT NULL,
  `description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `table_about_us`
--

INSERT INTO `table_about_us` (`id`, `description`) VALUES
(8, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sapien augue, congue id lorem ac, volutpat porttitor ex. Suspendisse at libero sed sapien pellentesque viverra vitae id nisi. Praesent in tellus in quam mollis vestibulum. Quisque nec');

-- --------------------------------------------------------

--
-- Table structure for table `table_feedback`
--

CREATE TABLE `table_feedback` (
  `id` int(11) NOT NULL,
  `username` varchar(220) NOT NULL,
  `email` varchar(220) NOT NULL,
  `phone` varchar(220) NOT NULL,
  `address` varchar(220) NOT NULL,
  `description` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `table_follow_us`
--

CREATE TABLE `table_follow_us` (
  `id` int(11) NOT NULL,
  `thumbnail` varchar(220) NOT NULL,
  `lable` varchar(220) NOT NULL,
  `url` text NOT NULL,
  `status` varchar(220) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `table_follow_us`
--

INSERT INTO `table_follow_us` (`id`, `thumbnail`, `lable`, `url`, `status`) VALUES
(36, '1079-528-social-icon.jpg', 'Facebook', 'https://www.facebook.com/', 'All'),
(44, '4424-Logo_of_Twitter.svg.png', 'twitter', '', 'All'),
(45, '5287-Instagram_logo_2016.svg.webp', 'instagram', '', 'Footer'),
(46, '2763-Logo_of_Twitter.svg.png', 'twitter', '', 'Footer'),
(47, '364-images.png', 'facebook', 'facebook.com', 'Footer');

-- --------------------------------------------------------

--
-- Table structure for table `table_logo`
--

CREATE TABLE `table_logo` (
  `id` int(11) NOT NULL,
  `thumbnail` varchar(220) NOT NULL,
  `status` varchar(220) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `table_logo`
--

INSERT INTO `table_logo` (`id`, `thumbnail`, `status`) VALUES
(32, '2765-Screenshot 2024-04-28 ypng.png', 'Header'),
(34, '8268-Screenshot 2024-04-28 18381.png', 'Footer');

-- --------------------------------------------------------

--
-- Table structure for table `table_news`
--

CREATE TABLE `table_news` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `banner` varchar(220) NOT NULL,
  `thumbnail` varchar(220) NOT NULL,
  `news_type` varchar(222) NOT NULL,
  `category` varchar(11) NOT NULL,
  `title` varchar(220) NOT NULL,
  `descrition` text NOT NULL,
  `viewer` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `table_news`
--

INSERT INTO `table_news` (`id`, `author_id`, `banner`, `thumbnail`, `news_type`, `category`, `title`, `descrition`, `viewer`, `created_at`) VALUES
(70, 13, '7295-photo_2024-04-27_20-16-41-768x1024.jpg', '5244-440429165_7484021405046516_921257448797572943_n.jpg', 'Social', 'National', '  សូមចូលរួមរំលែកទុក្ខ! យោធិន២០នាក់ បានពលីជីវិត ក្នុងឧបទ្ទវហេតុផ្ទុះគ្រាប់ នៅយោធភូមិភាគទី៣  ', '<p>ភ្នំពេញ៖ សម្តេចធិបតី ហ៊ុន ម៉ាណែត នាយករដ្ឋមន្ត្រីនៃកម្ពុជា បានសម្តែងក្តីរន្ធត់ក្តុកក្តួលឥតឧបមា ដែល បានទទួលដំណឹងអំពីឧបទ្ទវហេតុផ្ទុះគ្រាប់ នៅយោធភូមិភាគទី៣ ក្នុងខេត្តកំពង់ស្ពឺ កាលពីរសៀលថ្ងៃទី២៧ ខែមេសា ឆ្នាំ២០២៤ ដែលបានបង្កឱ្យយោធិនចំនួន ២០ នាក់ បានពលីជីវិត និងយោធិនមួយចំនួនទៀត បានរបួស។</p>', 1, '2024-04-28 11:04:59'),
(71, 13, '5548-kn3423_(13).jpg', '3713-kn3423_(1).png', 'Social', 'National', '  ខណ្ឌ័ ច័ន្ទសុផល ទម្លាយប្រវត្តិក្បាលតោនៅសាំងហ្គាពួរ និងផ្ទុះការរិះគន់ថា កម្ពុជាចម្លង Style គំនិតពីគេ!  ', '<p>រូបសំណាក &laquo;សេះសមុទ្រ&raquo; បាញ់ទឹកនៅខេត្តកំពត កំពុងទទួលបានការចាប់អារម្មណ៍យ៉ាងខ្លាំងពីប្រជាពលរដ្ឋចំពោះសមិទ្ធផលថ្មីនេះ ដោយអ្នកខ្លះមានគម្រោងទៅថតរូបលេងកម្សាន្តនៅឱកាសចូលឆ្នាំខាងមុខនេះទៀត។ ទោះយ៉ាងណា រូបសំណាកនេះបែរជាអ្នកមួយចំនួនតូចយកទៅTroll សើចចំអកឡកឡឺយទៅវិញ។ ឃើញដូចនេះតារាចម្រៀង ករុណា ពេជ្រ បានសម្តែងដោយការហួសចិត្តតែម្តងដោយលោកបាននិយាយថាអ្នកមួយចំនួនផ្តេកផ្តួលជោរតាមគ្នាហៅរូបសំណាកនេះជា Singapot ។</p>', 1, '2024-04-28 13:09:13'),
(74, 13, '648-6629ca35c6c18_1714014720_large.jpg', '1422-6629ca36d143e_1714014720_large.jpg', 'Sport', 'National', '  ដៃ​ធ្ងន់​ដូច​ ធឿន​ ធារ៉ា​ អីចឹង​ធ្វើ​បាន​ជោគជ័យ​ទេ​ប្រកួត​ Kick Boxing ​យប់​នេះ  ', '<p>កីឡាករ​ជើង​ខ្លាំង​ ធឿន​ ធារ៉ា​ នឹង​ធ្វើ​ការ​ប្រកួត​លក្ខណៈ​ Kick Boxing​ លើក​ដំបូង​នៅ​ថ្ងៃ​នេះ​ជា​មួយ​អ្នក​លេង​ក្បាច់​គុន​ចិន​ ខៃ ហ្សេបពីង ​ ក្នុង​កម្មវិធី​ GANZBERG​ រាជសីហ៍ KUN KHMER ​នៅ​ខណ្ឌ​កំបូល​ រាជធានី​ភ្នំពេញ។​</p>', 1, '2024-04-28 13:05:02'),
(75, 13, '3578-662b993e8550e_1714133280_large.jpg', '3049-662b993c552aa_1714133280_large.jpg', 'Sport', 'Internation', ' កាំភ្លើង​ធំ​ឈរ​លេខ ១ ប៉ុណ្ណោះ​អាសន្ន ហើយ​តាម​ប្រកិត​ពី​ក្រោយ​ដោយ​ Man City  ', '<p>ខណៈ​លីគ​ធំៗ​ដូច​ជា​អាល្លឺម៉ង់ និង​អ៊ីតាលី​រក​ឃើញ​ម្ចាស់​ពាន​ទៅ​ហើយ​នោះ លីគ​កំពូល​អង់គ្លេស​កំពុង​ស្ថិត​ក្នុង​ដំណាក់​កាល​មួយ​ប្រកួត​ប្រជែង​គ្នា​យ៉ាង​ក្ដៅ​គគុក ដោយ​បណ្ដា​ក្លឹប​កំពូល​តារាង​កំពុង​ប្រកៀក​ប្រកិត​គ្នា​យ៉ាង​ខ្លាំង។</p>\r\n<p>ក្នុង​លីគ​កំពូល​អង់គ្លេស មាន ២០ ក្រុម ហើយ​មួយ​រដូវ​កាល​មាន​ប្រកួត​សរុប ៣៨ ប្រកួត។</p>', 0, '2024-04-28 11:20:07'),
(76, 13, '9312-662b54086cb57_1714115580_large.jpg', '8293-662b54077fabf_1714115580_large.jpg', 'Sport', 'National', ' ចូលរួម​គាំទ្រ​ទាំង​អស់​គ្នា!​ ការ​ប្រកួត​ប្រដាល់​ស្វែង​រក​ថវិកា​ជួយ​កុមារ​កំព្រា​ ថ្ងៃ​ទី១០​ ឧសភា​ ២០២៤​  ', '<p>ការ​ប្រកួត​មិន​ធ្លាប់​មាន​ និង​កាន់​តែ​ពិសេស​មាន​គូ​ប្រកួត​ល្អៗ​ចម្រុះ​ជាតិ​សាសន៍​ក្នុង​កម្មវិធី​ Cambodia-Japan Charity Fight ​ថ្ងៃ​ទី១០​ ខែ​ឧសភា​ ឆ្នាំ​២០២៤​ ស្វែង​រក​ថវិកា​យក​ទៅ​ឧបត្ថម្ភ​មណ្ឌល​កុមារ​កំព្រា​មួយ​កន្លែង។​</p>', 0, '2024-04-28 11:22:24'),
(77, 13, '9529-662cae314ea0a_1714204200_large.jpeg', '1957-662cae349c137_1714204200_large.jpeg', 'Entertianment', 'National', ' អាយដល G-Devith ក៏ឈ្នះរង្វាន់ដែរ ឈ្នះ ១លានរៀល ពី វឌ្ឍនៈ ឡាហ្គឺរ៉េដ  ', '<p>ថ្មីៗនេះ កំពូលតារាចម្រៀងរ៉េបលោក&nbsp;<strong>G-Devith</strong>&nbsp;បានឈ្នះរង្វាន់ទឹកប្រាក់&nbsp;<strong>១លានរៀល</strong>&nbsp;ពីក្រវិលកំប៉ុង វឌ្ឍនៈ ឡាហ្គឺរ៉េដ ហើយអាយដល G-Devith រួមដំណើរជាមួយអាយដល​&nbsp;<strong>Top 5</strong>&nbsp;ស្រស់សង្ហា អ្នកឧកញ៉ា&nbsp;<strong>សំអាង វឌ្ឍនៈ</strong>&nbsp;ក៏បានយកសំណាងទៅឲ្យហ្វេនៗទាំងអស់គ្នាដែលគាំទ្រ និងចូលចិត្តអាយដលទាំងពីរ ក៏ដូចជារសជាតិថ្មីរបស់&nbsp;<strong>វឌ្ឍនៈ ឡាហ្គឺរ៉េដ</strong> ទៅដាក់តាមដេប៉ូចែកចាយជាច្រើនដែលជាដៃគូអាជីវកម្មរបស់ក្រុមហ៊ុន។</p>', 0, '2024-04-28 11:25:34'),
(78, 13, '4861-kn-945345_(9).jpg', '3905-kn-945345_(7).jpg', 'Social', 'National', ' ឯកឧត្តម ព្រាប កុល ប្រកាសគាំទ្រយ៉ាងពេញទំហឹងចំពោះគម្រោងព្រែកជីកហ្វូណន-តេជោ និងអំពាវនាវឱ្យពលរដ្ឋគ្រប់និន្នាការមានការសាមគ្គី និងឯកភាពជាតិ  ', '<p>ឯកឧត្តម ព្រាប កុល រដ្ឋមន្ដ្រីប្រតិភូអមនាយករដ្ឋមន្ដ្រី សូមប្រកាសគាំទ្រយ៉ាងពេញទំហឹង ចំពោះការអនុវត្តគម្រោងព្រែកជីកហ្វូណន-តេជោ ដែលជាព្រែកជីកប្រវត្តិសាស្ត្រលើកទី១នៅកម្ពុជា ដែលជាគំនិតផ្តួចផ្តើមរបស់សម្តេចអគ្គមហាសេនាបតីតេជោ ហ៊ុន សែន អតីតនាយករដ្ឋមន្ត្រី និងជាប្រធាន ព្រឹទ្ធសភានៃព្រះរាជាណាចក្រកម្ពុជា ហើយបច្ចុប្បន្នសម្តេចមហាបវរធិបតី ហ៊ុន ម៉ាណែត នាយករដ្ឋមន្ត្រីនៃព្រះរាជាណាចក្រកម្ពុជា បានបន្តស្នាដៃ និងជំរុញបន្ថែមក្នុងការអភិវឌ្ឍគម្រោង ព្រែកជីកហ្វូណន-តេជោឲ្យសម្រេចបាននាតាមផែនការនាពេលអនាគត។</p>', 0, '2024-04-28 11:27:38'),
(79, 13, '4736-6629ca701609d_1714014780_large.jpg', '7925-6629ca6e9f9ef_1714014780_large.jpg', 'Entertianment', 'Internation', ' ទៅមើល Exhuma ចាប់អារម្មណ៍តែតួប្រុសតួស្រី តិចមើលរំលងតួបិសាចមេទ័ពជប៉ុន រូបពិតសង្ហាមិនណយ  ', '<p><strong>Exhuma</strong> ភាពយន្ដខ្នាតធំរបស់កូរ៉េខាងត្បូងកំពុងអង្រួនវិស័យភាពយន្ននៅអាស៊ី ហើយភាគច្រើនអ្នកនិយមទស្សនាចាប់អារម្មណ៍ខ្លាំងទៅលើតួអង្គធំៗក្រុមគ្រូគាស់ផ្នូរ ហើយអាចនឹងមើលរំលងបិសាចមេទ័ព រូបពិតសង្ហាមិនធម្មតា សំខាន់ជាកីឡាករអាជីពក្នុងប្រទេសកូរ៉េទៀតផង។</p>', 0, '2024-04-28 11:30:11'),
(80, 13, '4278-6618bbae8fcbf_1712896920_large.jpg', '9433-6618bbb5c6a52_1712896920_large.jpg', 'Entertianment', 'National', '  LD Entertainment ដាក់បញ្ចូលសិល្បៈវប្បធម៌ខ្មែរច្រើនលើសលុប ក្នុងស្រឡាយរឿង «ប្រវត្តិស្នេហ៍នាងនាថ»  ', '<p>ក្រោយចំណាយពេលផលិតយ៉ាងសម្រិតសម្រាំងរយៈពេលជាច្រើន ទីបំផុត poster ទី១ នៃស្រឡាយរឿង &laquo;ប្រវត្តិស្នេហ៍នាងនាថ&raquo; ត្រូវបានទម្លាក់លើទីផ្សារឱ្យទស្សនិកជន និងអ្នកនិយមទស្សនាភាពយន្តបានឃើញហើយ។</p>', 1, '2024-04-28 13:04:55');

-- --------------------------------------------------------

--
-- Table structure for table `table_user`
--

CREATE TABLE `table_user` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(240) NOT NULL,
  `profile` varchar(220) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `table_user`
--

INSERT INTO `table_user` (`id`, `username`, `email`, `password`, `profile`) VALUES
(2, 'hengvanreuth', 'hengvanreuth.17@gmail.com', 'e1d5be1c7f2f456670de3d53c7b54f4a', '3085-photo_2023-03-07_08-05-40.jpg'),
(3, 'hengvanreuth', 'hengvanreuth.17@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '2838-photo_2022-09-06_21-37-20.jpg'),
(4, 'vanreuth', 'hengvanreuth.17@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '4911-photo_2023-03-07_08-05-40.jpg'),
(5, 'hcl', 'hengvanreuth.17@gmail.com', '202cb962ac59075b964b07152d234b70', '8608-photo_2022-09-06_21-37-16.jpg'),
(6, 'rith', 'hengvanreuth.17@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '4033-photo_2023-03-07_08-05-40.jpg'),
(7, 'hcl', 'hengvanreuth.17@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '6967-photo_2023-03-07_08-05-40.jpg'),
(8, 'hcl', 'hengvanreuth.17@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '5118-photo_2022-09-06_21-37-16.jpg'),
(9, 'vanreuth', 'hengvanreuth.17@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9507-photo_2023-03-07_08-05-40.jpg'),
(10, 'vanreuth', 'hengvanreuth.17@gmail.com', '96e79218965eb72c92a549dd5a330112', '1493-photo_2023-03-07_08-05-40.jpg'),
(11, 'vanreuth', 'hengvanreuth.17@gmail.com', '8b5700012be65c9da25f49408d959ca0', '9242-photo_2023-03-07_08-05-40.jpg'),
(12, 'vanreuth', 'hengvanreuth.17@gmail.com', '93279e3308bdbbeed946fc965017f67a', '1622-photo_2023-03-07_08-05-40.jpg'),
(13, 'thona', 'thona@gmail.com', '202cb962ac59075b964b07152d234b70', '1091-photo_2022-02-09_18-03-45.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table_about_us`
--
ALTER TABLE `table_about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_feedback`
--
ALTER TABLE `table_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_follow_us`
--
ALTER TABLE `table_follow_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_logo`
--
ALTER TABLE `table_logo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_news`
--
ALTER TABLE `table_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_user`
--
ALTER TABLE `table_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table_about_us`
--
ALTER TABLE `table_about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `table_feedback`
--
ALTER TABLE `table_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_follow_us`
--
ALTER TABLE `table_follow_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `table_logo`
--
ALTER TABLE `table_logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `table_news`
--
ALTER TABLE `table_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `table_user`
--
ALTER TABLE `table_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php 
    include('sidebar.php');
?>
                <div class="col-10">
                    <div class="content-right">
                        <div class="top">
                            <h3>Add Sport News</h3>
                        </div>
                        <div class="bottom">
                            <figure>
                                <form method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                        <label>File</label>
                                        <input type="file" name ="photo" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Lable</label>
                                        <input type="text" name="lable" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>URL</label>
                                        <input type="text" name="url" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Show on</label>
                                        <select class="form-select" name="status">
                                            <option value="All">All</option>
                                            <option value="Footer">Footer</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" name="btn_follow_us"class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
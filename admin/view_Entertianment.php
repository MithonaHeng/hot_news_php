<?php 
    include('sidebar.php');
?>
                <div class="col-10">
                    <div class="content-right">
                        <div class="top">
                            <h3>All Entertainment News</h3>
                        </div>
                        <div class="bottom view-post">
                            <figure>
                                <form method="post" enctype="multipart/form-data">
                                    <!-- <div class="block-search">
                                        <input type="text" class="form-control" placeholder="SEARCH HERE">
                                        <button type="submit">
                                        <img src="search.png" alt=""></button>
                                    </div> -->
                                    <table class="table" border="1px">
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>New type</th>
                                            <th>Categories</th>
                                            <th>Thumbnail</th>
                                            <th>Banner</th>
                                            <th>Viewer</th>
                                            <th>Post by</th>
                                            <th>Date</th>
                                            <th>Discription</th> 
                                            <th>Actions</th>
                                        </tr>
                                        <?php
                                        $cn  = new mysqli("localhost","root","","project_course");
                                        $sql ="SELECT t_news.*,t_user.username FROM `table_user` AS t_user INNER JOIN table_news AS t_news ON t_user.id = t_news.author_id WHERE `news_type`='Entertianment'"; 
                                        $rs  = $cn->query($sql);
                                        while($row = $rs ->fetch_assoc()){
                                        ?>
                                        <tr>
                                            <td><?php echo $row['id']; ?></td>
                                            <td style="
                                                    white-space: nowrap;
                                                    overflow: hidden;
                                                    text-overflow: ellipsis;
                                                    max-width: 200px;
                                            
                                            "><?php echo $row['title']; ?></td>
                                            <td><?php echo $row['news_type']; ?></td>
                                            <td><?php echo $row['category']; ?></td>
                                            <td><img width="70px" height="70px" src="assets/icon/<?php echo $row['thumbnail']; ?>"></td>
                                            <td><img width="70px" height="70px" src="assets/icon/<?php echo $row['banner'];?>"></td>
                                            <td><?php echo $row['viewer'] ?></td>
                                            <td><?php echo $row['username']; ?></td>
                                            <td><?php echo $row['created_at']; ?></td>
                                            <td style="
                                            white-space: nowrap;
                                            overflow:hidden;
                                            text-overflow:ellipsis;
                                            max-width: 100px;
                                            "><?php echo $row['descrition'];?></td>
                                            <td width="150px">
                                                <a  name ="edit" href="edit-sport-new.php?id=<?php echo $row['id']; ?>"class="btn btn-primary">Update</a>
                                                <button type="button" remove-id="<?php echo $row['id'];?>" class="btn btn-danger btn-remove" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                    Remove
                                                </button>
                                            </td>
                                        </tr>
                                        <?php 
                                        }
                                        ?>

                                        <!-- <tr>
                                            <td>Why do we use it?</td>
                                            <td>Sport</td>
                                            <td>National</td>
                                            <td><img src="https://via.placeholder.com/80"/></td>
                                            <td>27/June/2022</td>
                                            <td width="150px">
                                                <a href=""class="btn btn-primary">Update</a>
                                                <button type="button" remove-id="1" class="btn btn-danger btn-remove" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                    Remove
                                                </button>
                                            </td>
                                        </tr> -->
                                    </table>
                                    <ul class="pagination">
                                        <li>
                                            <a href="">1</a>
                                            <a href="">2</a>
                                            <a href="">3</a>
                                            <a href="">4</a>
                                        </li>
                                    </ul>

                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <h5 class="modal-title" id="exampleModalLabel">Are you sure to remove this post?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="" method="post">
                                                        <input type="hidden" class="value_remove" name="remove_id">
                                                        <button type="submit" class= "btn btn-danger" name="btn_delete_sport">Yes</button>
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>  
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
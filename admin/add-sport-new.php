<?php 
    include('sidebar.php');
?>
                <div class="col-10">
                    <div class="content-right">
                        <div class="top">
                            <h3>Add Sport News</h3>
                        </div>
                        <div class="bottom">
                            <figure>
                                <form method="post" enctype="multipart/form-data">
                                

                                    <div class="form-group" >
                                        <label>Title</label>
                                        <input type="text"name="title" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-select" name="new_type">
                                            <option value="Sport"readonly>Sport</option>
            
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>cateroty</label>
                                        <select class="form-select" name="category">
                                            <option value="National"readonly>National</option>
                                            <option value="International"readonly>International</option>
            
                                        </select>
                                    </div>
                
                                    <div class="form-group">
                                        <label>Photo</label>
                                        <input type="file" name="thamnail" class="form-control">
                                    </div>
                                    <div class="form-group" >
                                        <label>Banner</label>
                                        <input type="file" name="banner"class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn-sport" name ="btn-sport">Submit</button>
                                    </div>
                                </form>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
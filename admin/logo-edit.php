<?php 
    include('sidebar.php');
    $cn = new mysqli("localhost","root","","project_course");
    $id  = $_GET['id'];
    $sql = "SELECT * FROM `table_logo` WHERE id ='$id' LIMIT 1";
    $rs  = $cn->query($sql);
    $row = mysqli_fetch_assoc($rs);
    $select_footer ="";
    $select_header = "";
    if($row['status'] == "Footer"){
        $select_footer ="selected";
    }else{
        $select_header = "selected";
    }
?>
                <div class="col-10">
                    <div class="content-right">
                        <div class="top">
                            <h3>Add LOGO</h3>
                        </div>
                        <div class="bottom">
                            <figure>
                                <form method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                        <select name="status" id="" class="form-select">
                                            <option value="Header" <?php echo  $select_header?>>Header</option>
                                            <option value="Footer" <?php echo  $select_footer?>>Footer</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Photo</label>
                                        <input type="file" name="logo" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Old Photo</label><br>
                                       <img src="assets/icon/<?php echo $row['thumbnail']?>" width="70px" height="70px" alt="">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" name="btn_update" class="btn btn-primary">Update</button>
                                        <!-- <button type="submit" class="btn btn-success">Success</button>
                                        <button type="submit" class="btn btn-danger">Danger</button> -->
                                    </div>
                                </form>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
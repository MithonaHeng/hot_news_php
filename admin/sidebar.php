<?php
 include("function.php");
if(!empty($_SESSION["id"])){


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Dashboard</title>

    <!-- @theme style -->
    <link rel="stylesheet" href="assets/style/theme.css">

    <!-- @Bootstrap -->
    <link rel="stylesheet" href="assets/style/bootstrap.css">

    <!-- @script -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/bootstrap.js"></script>

    <!-- @tinyACE -->
    <script src="https://cdn.tiny.cloud/1/5gqcgv8u6c8ejg1eg27ziagpv8d8uricc4gc9rhkbasi2nc4/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

</head>
<body>
    <main class="admin">
        <div class="container-fluid">
            <div class="row">
                <div class="col-2">
                    <div class="content-left">
                        <div class="wrap-top">
                            <h5>Jong Deng News</h5>
                        </div>
                        <div class="wrap-center">
                            <?php
                           
                            // $id =  header("location:login.php");
                            $id    = $_SESSION["id"];
                            $sql  = "SELECT * FROM `table_user` WHERE id = '$id'";
                            $rs   = $cn->query($sql);
                            $row  = mysqli_fetch_assoc($rs);
                            
                            ?>
                            <img src="assets/icon/<?php echo $row['profile'] ?>" alt="" width="70px" height="70px">
                            <h6>Welcom to admin <?php echo $row['username'] ?></h6>
                        </div>

                        <div class="wrap-bottom">
                            <ul>
                                <ul>
                                <li class="parent">
                                    <a class="parent" href="javascript:void(0)">
                                        <span>SPORT NEW</span>
                                        <img src="assets/icon/arrow.png" alt="">
                                    </a>
                                    <ul class="child">
                                        <li>
                                            <a href="add-sport-new.php">Add Sport New</a>
                                            <a href="view-sport-new.php">View Sport New</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="parent">
                                    <a class="parent" href="javascript:void(0)">
                                        <span>SOCIAL</span>
                                        <img src="assets/icon/arrow.png" alt="">
                                    </a>
                                    <ul class="child">
                                        <li>
                                            <a href="add_social.php">Add Social</a>
                                            <a href="view_social.php">View Social</a>
                                        </li>
                                    </ul>
                                </li>

                                </li>
                                <ul>
                                <li class="parent">
                                    <a class="parent" href="javascript:void(0)">
                                        <span>ENTERTAINMENT</span>
                                        <img src="assets/icon/arrow.png" alt="">
                                    </a>
                                    <ul class="child">
                                        <li>
                                            <a href="add_entertianment.php">Add Entertainment New</a>
                                            <a href="view_Entertianment.php">View Entertainment New</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="parent">
                                    <a class="parent" href="javascript:void(0)">
                                        <span>LOGO</span>
                                        <img src="assets/icon/arrow.png" alt="">
                                    </a>
                                    <ul class="child">
                                        <li>
                                            <a href="logo-view.php">View Logo</a>
                                            <a href="logo-add.php">Add New</a>
                                        </li>
                                    </ul>
                                </li>
                         
                            </ul>

                            <ul>
                                <li class="parent">
                                    <a class="parent" href="view-feedback.php">
                                        <span>FEEDBACK</span>
                                        <img src="assets/icon/arrow.png" alt="">
                                    </a>
                                    
                                </li>
                                <ul>
                                <li class="parent">
                                    <a class="parent" href="javascript:void(0)">
                                        <span>FOLLOW US</span>
                                        <img src="assets/icon/arrow.png" alt="">
                                    </a>
                                    <ul class="child">
                                        <li>
                                            <a href="view_follow_us.php">View Follow us</a>
                                            <a href="add_follow_us.php">Add  Follow us</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="parent">
                                    <a class="parent" href="javascript:void(0)">
                                        <span>ABOUT</span>
                                        <img src="assets/icon/arrow.png" alt="">
                                    </a>
                                    <ul class="child">
                                        <li>
                                            <a href="view_about.php">View About</a>
                                            <a href="add_about.php">Add About</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="parent">
                                    <a class="parent" href="layout.php">
                                        <span>LOGOUT</span>
                                        <img src="assets/icon/arrow.png" alt="">
                                    </a>
                                </li>
                        </div>
                    </div>
                </div>
    <?php
    }else{
        header("location:login.php");
    }
    
    ?>
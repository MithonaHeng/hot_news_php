<!-- @import jquery & sweet alert  -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<?php 
$cn = new mysqli("localhost","root","","project_course");
function get_logo($logo_type){
    global $cn;
    $sql ="SELECT * FROM `table_logo` WHERE status='$logo_type' ORDER BY 'id' DESC LIMIT 1";
    $rs  = $cn->query($sql);
    $row = mysqli_fetch_assoc($rs);
    echo $row['thumbnail'];
}
function get_title(){
    global $cn;
    $sql="SELECT * FROM `table_news` ORDER BY 'id' DESC LIMIT 3";
    $rs  = $cn->query($sql);
    while($row = $rs->fetch_assoc()){
         $id = $row["id"];
        $title=$row['title'];
        echo'
        <i class="fas fa-angle-double-right"></i>
        <a href="news-detail.php?id='.$id.'">'.$title.'</a> &ensp;

        ';

    }
}
function get_news_detail($id_post){
    $id = $id_post;
    global $cn;
    $sql        ="SELECT * FROM `table_news` WHERE `id` = '$id' LIMIT 1";
    $rs         =$cn->query($sql);
   
    $row   = mysqli_fetch_assoc($rs);
       $thumneil    =$row['thumbnail'];
       $title       =$row['title'];
       $banner      =$row['banner'];
       $date        =$row['created_at'];
       $date        =date("D/M/Y",strtotime($date));
       $descrition= $row["descrition"];
       echo '
       <div class="main-news">
                         <div class="thumbnail">
                             <img width="730px"height="415px" src="../admin/assets/icon/'.$thumneil.'">
                         </div>
                         <div class="detail">
                             <h3 class="title">'.$title.'</h3>
                             <div class="date">'.$date.'</div>
                             <div class="description">
                                '.$descrition.'
                             </div>
                             <div class="thumbnail">
                             <img width="730px"height="415px" src="../admin/assets/icon/'.$banner.'">
                         </div>
                         </div>
                         </div>
       ';
    }
function get_news_type($id){
    global $cn;
    $sql   = "SELECT * FROM `table_news` WHERE id = '$id'";
    $rs    = $cn->query($sql);
    $row   = mysqli_fetch_assoc($rs);
    return $row["news_type"];
}
function get_related_new($id){
    $new_sport = get_news_type($id);
    global $cn;
    $sql   = "SELECT * FROM `table_news` WHERE news_type ='$new_sport' AND id NOT IN ($id) ORDER BY 'id' DESC LIMIT 3";
    $rs    = $cn->query($sql);
    while($row=$rs->fetch_assoc()){
        $banner = $row['banner'];
        $title  = $row['title'];
        $date        = $row['created_at'];
        $date        = date("d/M/Y",strtotime($date));
        $descrition= $row["descrition"];
        echo '
        <figure>
        <a href="news-detail.php?id='.$id.'">
            <div class="thumbnail">
            <img width="350px" hieght="200px" src="../admin/assets/icon/'.$banner.'">
            </div>
            <div class="detail">
                <h3 class="title">'.$title.'</h3>
                <div class="date">'. $date.'</div>
                <div class="description">
                    '.$descrition.'
                </div>
            </div>
        </a>
    </figure>
        
        ';
    }
}
function get_viewer($id){
         global $cn;
            $sql = "UPDATE `table_news` SET `viewer`=`viewer`+1  WHERE id='$id'";
            $rs  = $cn->query($sql);
     }
function get_most_viwer($type){
    global $cn;
    if($type == 'Trading'){
        $sql        = "SELECT * FROM `table_news` ORDER BY `viewer`DESC LIMIT 1";
    $rs         = $cn->query($sql);
    $row        = mysqli_fetch_assoc($rs);
    $thumneil   =$row['thumbnail'];
    $title      = $row['title'];
    $id         = $row['id'];

    echo'
         <div class="col-8 content-left">
            <figure>
                <a href="news-detail.php?id='.$id.'">
                    <div class="thumbnail">
                    <img width="730px"height="415px" src="../admin/assets/icon/'.$thumneil.'">
                        <div class="title">
                        '.$title.'
                        </div>
                    </div>
                </a>
            </figure>
        </div>
        ';
    }
    else{
        $sql="SELECT * FROM `table_news` WHERE id !=(SELECT id from `table_news` ORDER BY `viewer` DESC LIMIT 1) ORDER BY id DESC LIMIT 3 ";
        $rs         = $cn->query($sql);
        while( $row= $rs->fetch_assoc())
        {
            $banner     = $row['banner'];
            $title      = $row['title'];
            $id         = $row['id'];
            echo'
            <div class="col-12">
                        <figure>
                        <a href="news-detail.php?id='.$id.'">
                                <div class="thumbnail">
                                <img width="350px"height="200px" src="../admin/assets/icon/'.$banner.'">
                                <div class="title">
                                '.$title.'
                                </div>
                                </div>
                            </a>
                        </figure>
                 </div>
            
            
            ';
        }
    }
    
 }
function Sport_news(){
    global $cn;
    $sql = "SELECT * FROM `table_news` WHERE `news_type`='sport'";
    $rs  = $cn->query($sql);
    while( $row= $rs->fetch_assoc()){
            $banner     = $row['banner'];
            $title      = $row['title'];
            $id         = $row['id'];
        echo'
        <div class="col-4">
        <figure>
            <a href="news-detail.php?id='.$id.'">
                <div class="thumbnail">
                <img width="350px"height="200px" src="../admin/assets/icon/'.$banner.'">
                <div class="title">
                   '.$title.'
                </div>
                </div>
            </a>
        </figure>
    </div> 
        ';

    }
}
function Social_news(){
    global $cn;
    $sql = "SELECT * FROM `table_news` WHERE `news_type`='Social'LIMIT 3";
    $rs  = $cn->query($sql);
    while( $row= $rs->fetch_assoc()){
            $banner     = $row['banner'];
            $title      = $row['title'];
            $id         = $row['id'];
        echo'
        <div class="col-4">
        <figure>
            <a href="news-detail.php?id='.$id.'">
                <div class="thumbnail">
                <img width="350px"height="200px" src="../admin/assets/icon/'.$banner.'">
                <div class="title">
                   '.$title.'
                </div>
                </div>
            </a>
        </figure>
    </div> 
        ';

    }
}
function Entertainment_new(){
    global $cn;
    $sql="SELECT * FROM `table_news` WHERE `news_type`='Entertianment' LIMIT 3";
    $rs =$cn->query($sql);
    while($row=$rs->fetch_assoc()){
       $id       =$row['id'];
       $banner   =$row['banner'];
       $title    =$row['title'];
 
       echo' <div class="col-4">
       <figure>
       <a href="news-detail.php?id='.$id.'">
               <div class="thumbnail">
                  <img width="350px" height="200px" src="../admin/assets/icon/'.$banner.'" alt="">
               <div class="title">
                 '.$title.'
               </div>
               </div>
           </a>
       </figure>
   </div>';
 
    }
 }
function Search_News($query){
    global  $cn;
    $sql  = "SELECT * FROM `table_news` WHERE `title` LIKE '%$query%'";
    $rs   = $cn->query($sql);
    while( $row= $rs->fetch_assoc()){
            $banner         = $row['banner'];
            $title      = $row['title'];
            $id         = $row['id'];
            $date        =$row['created_at'];
            $date        =date("D/M/Y",strtotime($date));
            $descrition= $row["descrition"];

        echo '
        <div class="col-4">
            <figure>
                <a href="news-detail.php?id='.$id.'">
                    <div class="thumbnail">
                    <img width="350px"height="200px" src="../admin/assets/icon/'.$banner.'">
                    <div class="detail">
                        <h3 class="title">'.$title.'</h3>
                        <div class="date">'.$date.'</div>
                        <div class="description">
                            '.$descrition.'
                        </div>
                    </div>
                </a>
            </figure>
        </div>
        ';
    }

}
function list_sport_news($new_type,$category,$page,$limit){
     global $cn;
     $star = ($page -1)*$limit;
     $sql    = "SELECT * FROM `table_news` WHERE `news_type`='$new_type' AND `category`='$category' LIMIT $star,$limit";
     $rs  = $cn->query($sql);
     while( $row= $rs->fetch_assoc()){
        $banner         = $row['banner'];
            $title      = $row['title'];
            $id         = $row['id'];
            $date        =$row['created_at'];
            $date        =date("D/M/Y",strtotime($date));
            $descrition= $row["descrition"];
        
        echo '
        <div class="col-4">
            <figure>
                <a href="news-detail.php?id='.$id.'">
                    <div class="thumbnail">
                    <img width="350px"height="200px" src="../admin/assets/icon/'.$banner.'">
                    <div class="detail">
                        <h3 class="title">'.$title.'</h3>
                        <div class="date">'.$date.'</div>
                        <div class="description">
                            '.$descrition.'
                        </div>
                    </div>
                </a>
            </figure>
        </div>
                        ';

     }
}

function get_pageination($limit){
    global $cn;
    $sql   = "SELECT COUNT(id)as total_post FROM `table_news`";
    $rs    = $cn->query($sql);
    $row   = mysqli_fetch_assoc($rs);
    $total_post = $row ['total_post'];
    $pageination = $limit / $total_post;
    $pageination = ceil($pageination);
    for($i=1 ; $i<=$pageination ; $i++){
        echo '
        <ul>
            <li>
            <a href="?page='.$i.'">'.$i.'</a>
            </li>
         </ul>   ';

    }

}
function insert_contact(){
    global $cn;
    if(isset($_POST['btn_message'])){
    $name        = $_POST['username'];
    $phone       = $_POST['telephone'];
    $description = $_POST['message'];
    $email       = $_POST['email'];
    $address     = $_POST['address'];
    $sql = "INSERT INTO `table_feedback`(`id`, `username`, `email`, `phone`, `address`, `description`) VALUES (null,'$name','$email','$phone','$address','$description')";
    $rs          = $cn->query($sql);
    }

}
insert_contact();

function select_follow_us(){
    global $cn;
    $sql = "SELECT * FROM `table_follow_us` WHERE`status`='All' ";
    $rs  = $cn->query($sql);
    while($row=$rs->fetch_assoc()){
        $photo   = $row['thumbnail'];    
        $lable   = $row['lable'];
        $url     = $row['url'];
        echo'
        <li>
             <img src="../admin/assets/icon/'.$photo.'" width="40px"> <a href="'.$url.'">'.$lable.'</a>
        </li>
        
        ';
    }

}
function select_footer(){
    global $cn;
    $sql = "SELECT * FROM `table_follow_us` WHERE`status`='Footer' LIMIT 3";
    $rs  = $cn->query($sql);
    while($row=$rs->fetch_assoc()){
        $photo   = $row['thumbnail'];    
        $lable   = $row['lable'];
        $url     = $row['url'];
        echo'
        <li>
        <a href=""><img src="../admin/assets/icon/'.$photo.'" alt="" height="50px" width="50px"></a>

        </li>
        
        ';
    }

}
function get_about(){
    global $cn;
    $sql="SELECT * FROM `table_about_us`";
    $rs =$cn->query($sql);
    while($row=$rs->fetch_assoc()){
      $description=$row['description'];
        echo'
        '.$description.'
       
    ';
    }
 }  


?>
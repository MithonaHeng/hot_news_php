<footer>
        <div class="container">
            <div class="logo">
                <a href="">
                <img src="../admin/assets/icon/<?php get_logo("Footer")?>" width="120px" height="120px" alt="">
                </a>
            </div>
            <div class="about">
                <div class="description">
                  <?php get_about();?>
                </div>
            </div>
            <div class="connect">
                <ul>
                    <?php
                        select_footer();
                    
                    ?>
                    <!-- <li>
                        <a href=""><img src="https://dummyimage.com/40/27306D" alt=""></a>
                    </li>
                    <li>
                        <a href=""><img src="https://dummyimage.com/40/27306D" alt=""></a>
                    </li>
                    <li>
                        <a href=""><img src="https://dummyimage.com/40/27306D" alt=""></a>
                    </li> -->
                </ul>
            </div>
        </div>
    </footer> 
</body>
<!-- @slick slider -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<!-- @theme js -->
<script src="assets/script/theme.js"></script>
<!-- @funcy box -->
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>